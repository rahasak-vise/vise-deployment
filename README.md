# vise platfrom

vise is a custom [fedramp](https://www.fedramp.gov/) compliance auditing platform. 
<!---
this repository contains the deployment of `vise fedramp compliance` platform services.
-->


### fedramp

`fedramp` defines compliance requirements that need to satisfy when a company serving for an organization. for example, assume `vmasc` serving `dod`, fedramp defines compliance need to be satisfied by vmasc to serve dod. there are `17 security` controllers and `343 sub-controllers` in fedramp compliance. fedramp has three levels of severity(`low, mid, high`), depending on the serving organization the serverity level will differ. each serverity level have different number of controller rules(e.g `low level need to satisfy 100 controllers`, `high level need to satisfy all controllers` etc).


### functionality

vise platform needs to identify the severity level and the number of controllers needs to satisfy by the company to serve the organization. then these controllers will be stored in the `controller service`. to identity the controllers we need to define a way(e.g `scan a document`, `manually upload compliance rules`, `upload via csv file`). finally, vise needs to generate a compliance report about how the company gonna satisfy these controller rules. for that, various documents can be input and extract information via `nlp`, or the admin can `manually enter the information`. compliance report handling functions will be implemented on the `audit service`.

---

# vise implementation

### architecture

the following figure discussed the architecture of the vise platform.

![Alt text](vise-architecture.png?raw=true "vise platfrom architecture")


### services

following are the main services in the vise platfrom and their functionalities.

```
1. controller service - extract and store fedramp security controllers rules that need to be satisfied by the company to serve organization
2. audit service - handle compliance report generation functions
3. nlp service - handle nlp funcations and document blob storage
4. nginx load balancer - handle ssl and load balancing
5. gateway service - microservices api gateway service of the platform
6. auth service - authentication/authorization service
7. notifier service - notification handling service
```
